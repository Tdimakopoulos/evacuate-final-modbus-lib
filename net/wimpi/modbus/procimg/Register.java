package net.wimpi.modbus.procimg;

/**
 * Interface defining a register.
 * <p>
 * A register is read-write from slave and
 * master or device side.<br>
 * Therefor implementations have to be carefully
 * designed for concurrency.
 *
 */
public interface Register
    extends InputRegister {

  /**
   * Sets the content of this <tt>Register</tt> from the given
   * unsigned 16-bit value (unsigned short).
   *
   * @param v the value as unsigned short (<tt>int</tt>).
   */
  public void setValue(int v);

  /**
   * Sets the content of this register from the given
   * signed 16-bit value (short).
   *
   * @param s the value as <tt>short</tt>.
   */
  public void setValue(short s);

  /**
   * Sets the content of this register from the given
   * raw bytes.
   *
   * @param bytes the raw data as <tt>byte[]</tt>.
   */
  public void setValue(byte[] bytes);

}//interface Register
