package net.wimpi.modbus.procimg;

/**
 * Interface defining a digital input.
 * <p>
 * In Modbus terms this represents an
 * input discrete, it is read only from
 * the slave side.
 *
 */
public interface DigitalIn {

  /**
   * Tests if this <tt>DigitalIn</tt> is set.
   * <p>
   *
   * @return true if set, false otherwise.
   */
  public boolean isSet();
  
}//DigitalIn
