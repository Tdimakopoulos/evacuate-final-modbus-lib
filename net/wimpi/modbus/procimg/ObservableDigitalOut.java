package net.wimpi.modbus.procimg;

import net.wimpi.modbus.util.Observable;


/**
 * Class implementing an observable digital output.
 *
 */
public class ObservableDigitalOut
    extends Observable
    implements DigitalOut {

  /**
   * A boolean holding the state of this digital out.
   */
  protected boolean m_Set;

  public boolean isSet() {
    return m_Set;
  }//isSet

  public void set(boolean b) {
    m_Set = b;
    notifyObservers("value");
  }//set

}//class ObservableDigitalIn
