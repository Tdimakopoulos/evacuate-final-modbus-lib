package net.wimpi.modbus.procimg;

/**
 * Class implementing a simple <tt>Register</tt>.
 * <p>
 * The <tt>setValue()</tt> method is synchronized,
 * which ensures atomic access, but no specific access order.
 */
public class SimpleRegister
    extends SynchronizedAbstractRegister
    implements Register {

  /**
   * Constructs a new <tt>SimpleRegister</tt> instance.
   * It's state will be invalid.
   */
  public SimpleRegister() {
    m_Register = null;
  }//constructor

  /**
   * Constructs a new <tt>SimpleRegister</tt> instance.
   * 
   * @param b1 the first (hi) byte of the word.
   * @param b2 the second (low) byte of the word.
   */
  public SimpleRegister(byte b1, byte b2) {
    m_Register[0] = b1;
    m_Register[1] = b2;
  }//constructor

  /**
   * Constructs a new <tt>SimpleRegister</tt> instance
   * with the given value.
   *
   * @param value the value of this <tt>SimpleRegister</tt>
   *        as <tt>int</tt>.
   */
  public SimpleRegister(int value) {
    setValue(value);
  }//constructor(int)

}//SimpleInputRegister
