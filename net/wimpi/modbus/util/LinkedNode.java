package net.wimpi.modbus.util;

/**
 * Class defining a linked node element.
 *
 */ 
public class LinkedNode {

  protected Object m_Node;
  protected LinkedNode m_NextNode = null;

  public LinkedNode(Object node) {
    m_Node = node;
  }//constructor(Object)

  public LinkedNode(Object node, LinkedNode linkednode) {
    m_Node = node;
    m_NextNode = linkednode;
  }//constructor(Object,LinkedNode)

}//LinkedNode
