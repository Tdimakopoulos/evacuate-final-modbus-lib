package net.wimpi.modbus;

/**
 * Superclass of all specialised exceptions in
 * this package.
 *
 */
public class ModbusException
    extends Exception {

  /**
   * Constructs a new <tt>ModbusException</tt>
   * instance.
   */
  public ModbusException() {
    super();
  }//constructor

  /**
   * Constructs a new <tt>ModbusException</tt>
   * instance with the given message.
   * <p>
   * @param message the message describing this
   *        <tt>ModbusException</tt>.
   */
  public ModbusException(String message) {
    super(message);
  }//constructor

}//ModbusException
